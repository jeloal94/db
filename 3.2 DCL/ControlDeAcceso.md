##### En este archivo, se explicara las diferentes opciones de control de acceso que ofrece MYSQL.
Link para verlo en gitlab https://gitlab.com/jeloal94/db/-/tree/main/3.2%20DCL?ref_type=heads
### Creacion,modificacion y eliminacion de usuarios en Mysql

#### Creacion de un usuario

Para crear un usuario en MySQL hay un comando llamado **create user** en el que se nos creara el usuario y el parametro de **identified by** para asignarle una contraseña al usuario. Esto lo puedes hacer desde una API como el MySQL Workbench o desde la linea de comandos conectando a mysql con el usuario y la contraseña y indicando la direccion del servidor y el puerto,al no ser que sea localhost.

### Ejemplo de conexion indicando direccion del servidor y el puerto, desde terminal.
```sql
mysql -u root -p -h 192.168.4.254 -P 3306
```
### Ejemplo de conexion localhost
```sql
mysql -h localhost -u root -p
```
### Ejemplo de creacion de usuario usando create user
```sql
create user nombre_usuario identified by contraseña_usuario;
```
#### Modificacion de un usuario

Para modificar la contraseña de un usuario se usa el comando **alter user** y se necesita el 'usuario'@'Ip o desde que te conectas' y luego usaremos el parametro **identified by** y le ponemos la contraseña que nosotros queramos.

```sql
 alter user 'nombreUsuario'@'localhost' identified by 'NuevaContraseña';
```

Para cambiar el nombre de un usuario o host, usaremos este comando.Podemos usar **%** como comodin para el host si queremos que se pueda conectar en un rango de la ip.

```sql
RENAME USER 'usuario'@'%' TO 'pedro'@'%';
RENAME USER 'pedro'@'%' TO 'pedro'@'192.164.4.%';
```

Para la eliminacion de un usuario en mysql, primero eliminaremos todos sus privilegios usando el comando **revoke**, poniendo un **all** detras del revoke para eliminar todos sus privilegios, luego hay que poner **on** con las tablas y base de datos que tienes, aunque al querer eliminar ese usuario lo mejor seria usar *.* para eliminar sus privilegios de todas las tablas y bases de datos, luego usaremos el from para indicar el usuario.

```sql
 revoke all on *.* from 'pepe'@'localhost';
 ```

 Luego usaremos el comando **DROP** para eliminar el usuario.
 ```sql
  drop 'pepe'@'localhost';
 ```

### Como se autentican los usuarios y que opciones de autenticacion nos ofrece el SGBD.

MySQL tiene varios mecanismos para la autenticacion, te voy a mostrar los metodos de autenticacion mas comunes en MySQL.

#### Autenticacion basada en contraseña.

Este es el metodo mas comun y basico. Los usuarios tienen una combinacion de nombre de usuario y contraseña que utilizan para acceder al servidor MySQL.

```sql
CREATE USER 'usuario'@'localhost' IDENTIFIED BY 'contraseña';

GRANT ALL PRIVILEGES ON *.* TO 'usuario'@'localhost' WITH GRANT OPTION;

mysql -u usuario -p
```

#### Autenticacion basada en la autenticacion nativa del servidor MySQL.

Este metodo verifica las credenciales de usuario almacenadas en la base de datos de MySQL. Los usuarios y sus contraseñas se almacenan en la tabla de `mysql.user`.Este metodo ofrece opciones para encriptar contraseñas utilizando diferentes algoritmos de hash.

```sql
CREATE USER 'usuario'@'localhost' IDENTIFIED WITH mysql_native_password BY 'contraseña';

GRANT ALL PRIVILEGES ON *.* TO 'usuario'@'localhost' WITH GRANT OPTION;

mysql -u usuario -p
```

#### Autenticacion basada en el sistema operativo (OS Authentication).

En este metodo, MySQL confia en la autenticacion del sistema operativo para verificar las credenciales de los usuarios. Los usuarios son autenticados por el sistema operativo en el que se esta ejecutando el servidor MySQL.

Para este método, primero debes asegurarte de que el usuario del sistema operativo tenga privilegios de acceso a MySQL. Luego, el usuario puede iniciar sesión en MySQL sin necesidad de una contraseña

#### Autenticacion mediante certificados SSL/TLS.

MySQL permite la autenticacion de usuarios utilizando certificados SSL/TLS. Esto garantiza una comunicacion segura entre el cliente y el servidor MySQL, y la autenticacion se basa en la validez del certificado SSL/TLS.

Para configurar la autenticación SSL/TLS, primero necesitas generar un par de claves privada y pública, y luego configurar MySQL para usar estas claves. Una vez configurado, los clientes pueden autenticarse con el servidor utilizando certificados SSL/TLS.

#### Autenticacion basada en complementos (Plugin Authentication).
MySQL tambien admite la autenticacion mediante complementos(plugins). Algunos de los complementos disponibles incluyen `mysql_native_password`, `caching_sha2_password`,`sha256_password`, entre otros. Cada uno de estos complementos tiene sus propias caracteristicas y requisitos de configuracion.

```sql
CREATE USER 'usuario'@'localhost' IDENTIFIED WITH caching_sha2_password BY 'contraseña';

GRANT ALL PRIVILEGES ON *.* TO 'usuario'@'localhost' WITH GRANT OPTION;

mysql -u usuario -p

```

### Como mostrar los usuarios existentes y sus permisos.

Para mostrar los usuarios existentes y sus permisos en MySQL, solo tienes que hacer una consulta a la tabla **mysql.user** , esa tabla contiene informacion sobre los usuarios y los privilegios que tienen en MySQL.

```sql
SELECT *
FROM mysql.user;
```
Lo mas comodo es usar el comodin * para que te salgan todos los usuarios.

### Qué permisos puede tener un usuario y qué nivel de granularidad.

MySQL ofrece un sistema de control de acceso robusto que permite conceder a los usuarios diferentes niveles de permisos para realizar diversas operaciones en las bases de datos y tablas.

#### Privilegios globales

Los privilegios globales se aplican a nivel de servidor y afectan a todas las bases de datos.

**CREATE USER** : Permite al usuario crear nuevas cuentas de usuario.

**CREATE** : Permite al usuario crear nuevas bases de datos y tablas.

**DROP** : Permite al usuario eliminar filas de una tabla.

**DELETE** : Permite al usuario insertar nuevas filas en una tabla.

**INSERT** : Permite al usuario insertar nuevas filas en una tabla.

**SELECT** : Permite al usuario leer datos de las tablas.

**UPDATE** : Permite al usuario actualizar filas en una tabla.

**GRANT OPTION** : Permite al usuario otorgar o revocar privilegios a otros usuarios.

**ALL PRIVILEGES** : Concede todos los privilegios disponibles para una base de datos especifica o todas las bases de datos.

**SUPER** : Concede privilegios de administracion, como la capacidad de iniciar o detener el servidor MySQL.

#### Privilegios especificos de la base de datos.

Los privilegios se aplican a nivel de base de datos.

**ALTER** : Permite al usuario modificar la estructura de una tabla.
**CREATE TEMPORARY TABLES** : Permite al usuario crear tablas temporales.
**CREATE VIEW** : Permite al usuario crear vistas.
**EVENT** : Permite al usuario programar eventos del servidor.
**TRIGGER** : Permite al usuario crear y eliminar disparadores en tablas.
**SHOW VIEW** : Permite al usuario ver las definiciones de las vistas.

#### Privilegios especificos de la tabla.

Los privilegios se aplican a nivel de tabla.

**DELETE** : Permite al usuario eliminar filas de una tablas especifica.


**INSERT** : Permite al usuario insertar nuevas filas en una tabla especifica.


**SELECT** : Permite al usuario leer datos de una tabla especifica.

**UPDATE** : Permite al usuario actualizar filas en una tabla especifica.

MySQL ofrece un alto nivel de granularidad en la gestión de permisos, permitiendo asignar privilegios a nivel global, de base de datos o de tabla, y también permite especificar los privilegios para usuarios individuales o para roles. Esta granularidad proporciona un control detallado sobre quién puede acceder y realizar operaciones en los diferentes objetos de la base de datos.

### Qué permisos hacen falta para gestionar los usuarios y sus permisos.

Necesitas tener ciertos privilegios especificos que te permitan realizar estas operaciones de administracion.

**CREATE USER** - Este privilegio permite al usuario actual crear nuevas cuentas de usuario.


**GRANT OPTION** -  Este privilegio permite al usuario actual otorgar o revocar privilegios a otros usuarios. Es necesario para que un usuario pueda asignar permisos a otros usuarios.


**RELOAD** - Este privilegio permite al usuario recargar los archivos de privilegios y vaciar las tablas de caché de privilegios, lo que puede ser necesario después de cambiar los privilegios de usuario.



**SELECT** en la tabla 'mysql.user' - Este privilegio es necesario para ver información sobre los usuarios existentes y sus privilegios.


**SHOW DATABASES** -  Este privilegio permite al usuario ver la lista de bases de datos en el servidor MySQL, lo que puede ser útil para gestionar permisos a nivel de base de datos.


**ALL PRIVILEGES** a nivel global, de base de datos o de tabla - Si se desea que un usuario tenga control total sobre los privilegios de otros usuarios, se puede conceder el privilegio ALL PRIVILEGES a nivel global, de base de datos o de tabla. Sin embargo, este privilegio otorga control total sobre los objetos correspondientes y debe concederse con precaución.


### Agrupacion de usuarios en MySQL

**Roles personalizados**: Se pueden crear roles personalizados que representen ciertos grupos de usuarios y luego otorgar permisos a esos roles.Por ejemplo, podriamos tener un rol llamado **administrador** que tenga privilegios de administracion, y luego asignar ese rol a los usuarios que necesiten esos privilegios.

```sql
CREATE ROLE administrador;
GRANT ALL PRIVILEGES ON *.* TO administrador;
GRANT administrador TO 'maria'@'localhost';
GRANT administrador TO 'pepe'@'%';
```

**Grupos de usuarios implicitos**: Podemos lograr cierta agrupacion implicita de usuarios al definir los permisos de manera que se apliquen a multiples usuarios.Por ejemplo, si deseas que varios usuarios tengan los mismos permisos, puedes otorgar esos permisos individualmente a cada usuario, o bien crear un rol y asignar los permisos al rol, luego asignar ese rol a los usuarios.

**Permisos basados en patrones de nombre de usuario o host**: MySQL tambien permite otorgar permisos basados en patrones de nombre de usuario o host, lo que podria ser util si deseas aplicar ciertos permisos a todos los usuarios que coincidan con un patron especifico.

En resumen, aunque MySQL no tiene una funcionalidad de agrupación de usuarios directa como algunos otros sistemas de bases de datos, puedes lograr una funcionalidad similar utilizando roles personalizados, asignación de permisos a través de patrones, y asignación de permisos a múltiples usuarios de manera individual.

### Comandos usados para gestionar los usuarios y sus permisos
Para gestionar usuarios y sus permisos en MySQL,podemos usar varios comandos, aqui pongo unos cuantos comandos.

**CREATE USER**: Para crear un nuevo usuario en MySQL.
```sql
CREATE USER 'usuario'@'localhost' IDENTIFIED BY 'contraseña';
```

**GRANT**: Para otorgar privilegios a un usuario en MySQL.
```sql
GRANT privilegios ON basededatos.tabla TO 'usuario'@'localhost';
```

**REVOKE**: Para revocar privilegios de un usuario.
```sql
REVOKE privilegios ON basededatos.tabla TO 'usuario'@'localhost';
```

**DROP USER**: Para eliminar un usuario.
```sql
DROP USER 'usuario'@'localhost';
```

**FLUSH PRIVILEGES** Para aplicar cambios en los privilegios.
```sql
FLUSH PRIVILEGES;
```
Si estas en terminal y quieres volver a repetir un comando que ya hayas usado anteriormente puedes usar control+R para activar el rollback y poner el principio del comando y te saldra un comando que hayas hecho anteriormente con ese principio del comando.
