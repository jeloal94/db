En este Readme estan los querys del ejercicio 2-1 explicados como se han realizado.
#### Link al Git Lab de este ejercicio por Jesus Lopez Alacot
https://gitlab.com/jeloal94/db/-/tree/main/Reto%202-1?ref_type=heads
### Query 1
### Encuentra todos los clientes de Francia.
```sql
use Chinook;

SELECT *
FROM Customer
where Country = 'France';
```
Este ejercicio como no especificaba datos, he puesto que diga todo sobre los clientes pertenecientes a Francia.Lo unico que habia que hacer para encontrarlos era usar un **Where** con la **columna** de country buscando que los datos de esa columna sean iguales a la condicion que hemos puesto que es France entre comillas simples para que detecte que es un varchar, ya que Country usa varchar como tipo de dato.

### Query 2
### Muestra las facturas del primer trimestre de este año.
```sql
use Chinook;

SELECT *
FROM Invoice
WHERE InvoiceDate BETWEEN CONCAT(YEAR(CURRENT_DATE()),'-01-01') AND CONCAT(YEAR(CURRENT_DATE()), '-03-31');
```
En este ejercicio tambien al no indicar que datos mostrar he hecho que muestren todos los datos de la tabla Invoice(Factura).He usado en el **Where** un **BETWEEN** y la funcion de **CONCAT** en el que dentro he puesta la funcion **YEAR** para referirme al año con la funcion de **CURRENT_DATE** para usar la fecha actual y usando el mes 1 y el dia 1, y luego lo mismo pero con el mes 3 y el dia 31 para que me coja el primer dia del primer mes del año actual y el ultimo dia del tercer mes del año actual. Esta query se puede usar todos los años.

### Query 3
### Muestra todas las canciones compuestas por AC/DC.
```sql
use Chinook;

SELECT Name, Composer
FROM Track
WHERE Composer = "AC/DC";
```
En este ejercicio he sacado el nombre y el compositor de las canciones para verificar que el nombre de las canciones y su compositor son AC/DC.Hemos usado un **Where** donde buscaremos que los datos de la columna Composer concuerden con los datos que le hemos especificado usando un varchar con el dato de "AC/DC" y asi conseguimos mostrar todas las canciones compuestas por AC/DC.

### Query 4
### Muestra las 10 canciones que más tamaño ocupan.
```sql
use Chinook;

Select Name,Bytes
FROM Track
ORDER BY BYTES DESC
LIMIT 10;
```
En este ejercicio he sacado el nombre de la cancion y los Bytes que ocupa para saber el nombre de la cancion y su tamaño en Bytes.He usado un **ORDER BY** con los Bytes usando **DESC** para que me los ordene de mayor a menor y usando **LIMIT** con el numero de 10 para que solo me salgan las 10 primeras canciones que mas tamaño ocupan.

### Query 5
### Muestra el nombre de aquellos países en los que tenemos clientes.
```sql
use Chinook;

SELECT Country
FROM Customer
WHERE Country is not null
```

En este ejercicio solo he mostrado la columna de Country porque solo queremos saber en que paises tenemos clientes,ninguna informacion mas. Para descubrirlo he usado en el **WHERE** la comparacion **is not null** porque asi solo me salen los paises en los que si tenemos clientes, porque son paises que no estaran con el dato de null.

### Query 6
### Muestra todos los géneros musicales.
```sql
use Chinook;

SELECT Name
FROM Genre
```

En este ejercicio he mostrado todos los generos musicales de la tabla **Chinook**.

### Query 7
### Muestra todos los artistas junto a sus álbumes
```sql
use Chinook;

SELECT a.Name, al.Title
FROM Artist a JOIN Album al ON a.ArtistId = al.ArtistId
ORDER BY a.ArtistId;
```

En este ejercicio he realizado un **INNER JOIN** de la tabla de **Artista** a la de **Album** por la id de artista y la he ordenado por la id de la tabla **Artista** y luego he seleccionado para leer los datos de la columna de la tabla **Artista** la columna de **nombre** y de la tabla de **Album** la columna de titulo.

### Query 8
### Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen.

```sql
use Chinook;

SELECT e.EmployeeID AS "Empleado ID", e.FirstName AS "Nombre Empleado",e.BirthDate AS "Fecha Nacimiento" ,e1.EmployeeID AS "Supervisor Id", e1.FirstName AS "Supervisor Nombre"
FROM Employee e JOIN Employee e1 ON e.EmployeeId = e1.ReportsTo
ORDER BY e.BirthDate asc
LIMIT 15;
```

Para la realizacion de este ejercicio, realize un inner join a si mismo usando como conectores la id de la tabla **Employee** y la columna de **ReportsTo**, y ordenede por la columna de **BirthDate** de forma ascendente para que me salieran desde los mas jovenes, a los mas viejos con un limite de 15 para que solo nos salgan los 15 empleados mas jovenes, y luego busque leer las columnas de **EmployeeID**, la columna de **FirstName** y la columna de **BirthDate** de la primera tabla, y de la segunda tabla **EmployeeID** y el **FirstName** de los supervisores.

### Query 9
### Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación,código postal, país, importe (en este orden).

```sql
use Chinook;

SELECT DISTINCT i.InvoiceDate,c.FirstName,c.LastName, i.BillingAddress,c.PostalCode,c.Country,i.Total
FROM Customer as c join Invoice i ON c.CustomerId = i.CustomerId
WHERE City = "Berlin";
```

Para la realizacion de este ejercicio, realize un inner join de la tabla **Customer**, a la tabla **Invoice** usando la id de la tabla **Customer** como conector, y como necesitamos de una ciudad en concreto he usado el **WHERE** con la columna **City** que concuerde con **Berlin**, y lei las columnas de la tabla **Invoice**, **InvoiceDate** para saber la fecha de la facturacion, **BillingAddress** para saber la direccion de envio de la factura,y la columna **Total** para saber el precio total de su factura, de la tabla **Customer** lei las columnas de **FirstName** para saber el nombre, la columna de **LastName** para saber los apellidos de los clientes y asi poder diferenciar los clientes, por si tuvieran el mismo nombre, la columna de **PostalCode** para saber su codigo postal, y la columna de **Country** para saber de que pais son.

### Query 10
### Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración.

```sql
use Chinook;

SELECT p.Name, t.Name
FROM Playlist as p JOIN PlaylistTrack as pl ON p.PlaylistId = pl.PlaylistId
JOIN Track as t ON pl.TrackId = t.TrackId
WHERE p.Name like 'C%'
ORDER BY t.AlbumId,t.Milliseconds;
```

Para la realizacion de este ejercicio, realize un par de **Inner Join** desde la tabla **Playlist** a la tabla de **PlaylistTrack** usando la id de **PlayLIst** y luego de la tabla **PlaylistTrack** a la tabla **Track** usando la id de la tabla **Track**, use el **Where** con la columna de **Name** de la tabla Playlist y usando el **like 'C%'** para buscar todos los nombres de la listas de reproduccion que comiencen por la letra C y luego use un **Order by** usando la columna de **AlbumId** de la tabla **Track** y la columna de **Milliseconds** de la tabla de **Track**, luego solo busque leer el nombre de la playlist, y el nombre del Track.

### Query 11
### Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.

```sql
use Chinook;

SELECT c.FirstName as "Nombre",c.LastName as "Apellido",i.Total as "Compras"
FROM Customer as c JOIN Invoice as i ON c.CustomerId = i.CustomerId
WHERE i.Total >= 10
ORDER BY c.LastName;
```

Para la realizacion de este ejercicio, realize un **Inner Join** desde la tabla **Customer**, a la tabla **Invoice** usando la id de la tabla **Customer**, use un **Where** usando la columna **Total** de la tabla Invoice para que solo me buscara pedidos por valor superiores de 10 euros, y use un **Order By** de la columna **LastName** de la tabla **Customer** para que me los ordene por apellidos, luego solo lei las tablas de **FirstName** y **LastName** de la columna Customer y el **Total** de la columna **Invoice**.

### Query 12
### Muestra el importe medio, mínimo y máximo de cada factura
```sql
use Chinook;

SELECT c.FirstName,c.LastName ,AVG(i.Total) AS "Media de la Factura", MIN(i.Total) AS "El minimo de la factura", MAX(i.Total) AS "El maximo de la factura"
FROM Invoice as i JOIN Customer c ON i.CustomerId = c.CustomerId
GROUP BY c.CustomerId;
```

Para la realizacion de este ejercicio, hice un **Inner Join** desde la tabla **Invoice** a la tabla **Customer** usando la id de la tabla **Customer**, use un **Group By** con la columna de **CustomerId** de la tabla **Customer**, luego solo lei las columnas de **FirstName** y **LastName** de la tabla **Customer**, y luego use el comando **AVG** para hacer la media de la columna **Total** de la tabla **Invoice** para saber la media de las facturas de cada cliente, luego el comando **MIN** con la columna de **Total** de la tabla **Invoice** para saber el precio minimo de las facturas de cada cliente, y por ultimo, use el comando **MAX** con la columna de **Total** de la tabla **Invoice** para saber el precio maximo de las facturas de cada cliente.

### Query 13
### Muestra el número total de artistas
```sql
use Chinook;

SELECT COUNT(ArtistId) as "Numero de Artistas"
FROM Artist;
```

Para la realizacion de este ejercicio, simplemente use el comando **COUNT** de la columna **ArtistId** para saber el numero de artistas que tenemos la tabla Artist.

### Query 14
### Muestra el número de canciones del álbum “Out Of Time”.
```sql
SELECT COUNT(t.Name) AS "Canciones del Album"
FROM Album AS a JOIN Track as t ON a.AlbumId = t.AlbumId
WHERE a.Title = "Out of Time";
```

Para la realizacion de este ejercicio, hice un **Inner Join** de la tabla **Album** a la tabla **Track** usando la id de la tabla **AlbumId**, luego use un **WHERE** con la columna **Title** de la tabla **Album** buscando que el titulo del album sea "Out of time" y luego hacer un **count** de la columna **Name** de la tabla **Track** para saber el numero de canciones totales del album "Out of time".

### Query 15
### Muestra el número de países donde tenemos clientes.
```sql
use Chinook;

SELECT COUNT(Country) AS "Paises"
FROM Customer
WHERE Country is not null;
```

La realizacion de este ejercicio, solo tuve que poner un **WHERE** con la columna **Country** donde el resultado no sea **null** para que solo nos muestre paises donde tenemos clientes y luego solo hice un **COUNT** de la columna **COUNTRY** para saber cuantos paises tenemos con clientes.

### Query 16
### Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).
```sql
use Chinook;

SELECT COUNT(t.genreId) as "Id Genero", g.Name
FROM Genre as g JOIN Track as t ON g.GenreId = t.GenreId
GROUP BY t.genreId;
```

La realizacion de este ejercicio, hice un **Inner Join** de la tabla **Genre** a la tabla **Track** usando la id de **Genre**, hice un **GROUP BY** de **GenreId** de la tabla **Track**, y luego hice un **COUNT** de **genreId** para saber el numero de canciones con ese genero y tambien mostre la columna de **Name** de la tabla **Genre** para saber el nombre de cada genero.

### Query 17
### Muestra los álbumes ordenados por el número de canciones que tiene cada uno.
```sql
use Chinook;

SELECT a.Title, COUNT(t.Name) AS "Nº Canciones"
FROM Album as a JOIN Track as t ON a.Albumid = t.Albumid
GROUP BY a.AlbumId
ORDER BY COUNT(t.Name) desc;
```

Para la realizacion de este ejercicio, hice un **INNER JOIN** desde la tabla **Album** a la tabla **Track** usando la id de **Album** como conectores, hice un **GROUP BY** de la columna **AlbumId** de la tabla **Album**, y luego hice un **ORDER BY** usando un **COUNT** de la columna **Name** de la tabla **Track** de forma desc, y luego mostre la columna **Title** de la tabla **Album** y tambien hice un **COUNT** de la columna **Name** de la tabla **Track**.

### Query 18
### Encuentra los géneros musicales más populares (los más comprados).
```sql
use Chinook;

SELECT DISTINCT g.Name
FROM InvoiceLine as i JOIN Track as t ON i.TrackId = t.TrackId
JOIN Genre as g ON t.GenreId = g.GenreId
GROUP BY i.TrackId; 
```

Para la realizacion de este ejercicio, hice un **INNER JOIN** desde la tabla **InvoiceLine** a la tabla **Track** usando la id de **Track** como conectores, y luego hice otro **Inner Join** desde **Track** a la tabla **Genre** usando la id de **Genre** como conectores, luego hice un **GROUP BY** de la columna de **TrackId** de la tabla **InvoiceLine** y luego mostre la columna **Name** de la tabla **Genre**.

### Query 19
### Lista los 6 álbumes que acumulan más compras.
```sql
use Chinook;

SELECT a.AlbumId, a.Title
FROM Album as a JOIN Track as t ON a.AlbumId = t.AlbumId
JOIN InvoiceLine as i ON t.TrackId = i.TrackId
GROUP BY a.AlbumId, a.Title
ORDER BY MIN(i.InvoiceId)
LIMIT 6; 
```

Realize este ejercicio, realizando dos **INNER JOIN**, el primer **JOIN** lo hice de la tabla **Album** a la tabla **Track** usando la id de **Album** como conectores, el segundo **JOIN** hice desde la tabla **Track** a la tabla **InvoiceLine** usando la id de **Track** como conectores, luego hice un **GROUP BY** de la columna de **AlbumId** y de la columna **Title** de la tabla Album, y luego hice un **ORDER BY** con el **MIN** de la columna **InvoiceId** y puse un **LIMIT** de 6, y luego mostre las columnas de **AlbumId** y **Title** de la tabla **Album**.

### Query 20
### Muestra los países en los que tenemos al menos 5 clientes.
```sql
use Chinook;

SELECT Country,COUNT(CustomerId) AS "NºClientes"
FROM Customer
WHERE Country IN (
    SELECT Country
    FROM Customer
    GROUP BY Country
    HAVING COUNT(*) >= 5
)
GROUP BY Country
ORDER BY Country ASC;
```
Este ejercicio, realize un **WHERE** de la columna **Country** y use el comando **IN** haciendo una subconsulta y en esa subconsulta hice un **GROUP BY** de la columna **Country** y hice un **HAVING** donde hice un **COUNT** de todo y que sea mayor o igual a 5. Luego fuera de la subconsulta hice un **GROUP BY** de la columna **COUNTRY** y hice un **ORDER BY** de la columna **Country** ascendente y luego mostre la columna **Country** y hice un **COUNT** de la columna **CustomerId** para saber cuantos clientes tenemos.