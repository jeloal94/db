use Chinook;

SELECT p.Name, t.Name
FROM Playlist as p JOIN PlaylistTrack as pl ON p.PlaylistId = pl.PlaylistId
JOIN Track as t ON pl.TrackId = t.TrackId
WHERE p.Name like 'C%'
ORDER BY t.AlbumId,t.Milliseconds;

