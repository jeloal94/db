use Chinook;

SELECT c.FirstName as "Nombre",c.LastName as "Apellido",i.Total as "Compras"
FROM Customer as c JOIN Invoice as i ON c.CustomerId = i.CustomerId
WHERE i.Total >= 10
ORDER BY c.LastName;