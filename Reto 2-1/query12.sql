use Chinook;

SELECT c.FirstName,c.LastName ,AVG(i.Total) AS "Media de la Factura", MIN(i.Total) AS "El minimo de la factura", MAX(i.Total) AS "El maximo de la factura"
FROM Invoice as i JOIN Customer c ON i.CustomerId = c.CustomerId
GROUP BY c.CustomerId;
