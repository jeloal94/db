use Chinook;


SELECT COUNT(t.Name) AS "Canciones del Album"
FROM Album AS a JOIN Track as t ON a.AlbumId = t.AlbumId
WHERE a.Title = "Out of Time";