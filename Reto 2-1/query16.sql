use Chinook;

SELECT COUNT(t.genreId) as "Id Genero", g.Name
FROM Genre as g JOIN Track as t ON g.GenreId = t.GenreId
GROUP BY t.genreId;