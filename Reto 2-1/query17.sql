use Chinook;

SELECT a.Title, COUNT(t.Name) AS "Nº Canciones"
FROM Album as a JOIN Track as t ON a.Albumid = t.Albumid
GROUP BY a.AlbumId
ORDER BY COUNT(t.Name) desc;