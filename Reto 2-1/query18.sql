use Chinook;

SELECT DISTINCT g.Name
FROM InvoiceLine as i JOIN Track as t ON i.TrackId = t.TrackId
JOIN Genre as g ON t.GenreId = g.GenreId
GROUP BY i.TrackId; 