use Chinook;

SELECT a.AlbumId, a.Title
FROM Album as a JOIN Track as t ON a.AlbumId = t.AlbumId
JOIN InvoiceLine as i ON t.TrackId = i.TrackId
GROUP BY a.AlbumId, a.Title
ORDER BY MIN(i.InvoiceId)
LIMIT 6; 