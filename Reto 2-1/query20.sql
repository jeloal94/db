use Chinook;

SELECT Country,COUNT(CustomerId) AS "NºClientes"
FROM Customer
WHERE Country IN (
    SELECT Country
    FROM Customer
    GROUP BY Country
    HAVING COUNT(*) >= 5
)
GROUP BY Country
ORDER BY Country ASC;
