use Chinook;

SELECT a.Name, al.Title
FROM Artist a JOIN Album al ON a.ArtistId = al.ArtistId
ORDER BY a.ArtistId;