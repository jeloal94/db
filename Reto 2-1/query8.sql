use Chinook;

SELECT e.EmployeeID AS "Empleado ID", e.FirstName AS "Nombre Empleado",e.BirthDate AS "Fecha Nacimiento" ,e1.EmployeeID AS "Supervisor Id", e1.FirstName AS "Supervisor Nombre"
FROM Employee e JOIN Employee e1 ON e.EmployeeId = e1.ReportsTo
ORDER BY e.BirthDate asc
LIMIT 15;