use Chinook;

SELECT DISTINCT i.InvoiceDate,c.FirstName,c.LastName, i.BillingAddress,c.PostalCode,c.Country,i.Total
FROM Customer as c join Invoice i ON c.CustomerId = i.CustomerId
WHERE City = "Berlin";