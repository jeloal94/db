use Chinook;

SELECT c.FirstName, (SELECT SUM(il.UnitPrice * il.Quantity)
FROM Invoice AS i JOIN InvoiceLine as il ON i.InvoiceId = il.InvoiceId
WHERE i.CustomerId = c.CustomerId) as "Total Gastado"
FROM Customer as c;