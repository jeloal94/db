use Chinook;

SELECT e.FirstName, (SELECT COUNT(i.InvoiceId) FROM Invoice AS i JOIN
Customer as c ON i.CustomerId = c.CustomerId WHERE c.SupportRepId = e.EmployeeId) as Ventas
FROM Employee as e;