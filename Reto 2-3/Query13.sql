USE Chinook;

SELECT a.Title, (SELECT COUNT(TrackId) FROM Track as t WHERE t.AlbumId = a.AlbumId) as Canciones
FROM Album as a;