use Chinook;

SELECT ar.Name, a.Title
FROM Artist as ar JOIN(SELECT AlbumId,ArtistId,Title FROM Album ORDER BY AlbumId DESC) as a ON ar.ArtistId = a.ArtistId;