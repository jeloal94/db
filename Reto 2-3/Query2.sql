use Chinook;

SELECT *
FROM Invoice 
WHERE CustomerId = (SELECT CustomerId FROM Customer WHERE Email = "emma_jones@hotmail.com")
ORDER BY InvoiceDate DESC
LIMIT 5;