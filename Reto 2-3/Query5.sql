use Chinook;

SELECT a.Title, ar.Name
FROM Album as a JOIN Artist ar ON a.ArtistId = ar.ArtistId
WHERE a.AlbumId IN (SELECT AlbumId FROM Track GROUP BY AlbumId HAVING COUNT(TrackId) > 15);