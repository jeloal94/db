use Chinook;

SELECT DISTINCT a.*
FROM Album as a JOIN Track as t ON a.AlbumId = t.AlbumId
WHERE a.AlbumId IN (SELECT AlbumId FROM Track GROUP BY TrackId HAVING TrackId >= AVG(TrackId));
