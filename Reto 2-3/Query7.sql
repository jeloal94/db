use Chinook;

SELECT DISTINCT a.*
FROM Album as a JOIN Track as t ON a.AlbumId = t.AlbumId
WHERE a.AlbumId IN (SELECT Albumid FROM Track Group by AlbumId HAVING Milliseconds > AVG(milliseconds));