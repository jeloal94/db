use Chinook;

SELECT t.Name
FROM Track as t JOIN Genre as g ON t.GenreId = g.GenreId
WHERE t.GenreId IN(SELECT GenreId FROM Genre group by GenreId HAVING GenreId > COUNT(GenreId));