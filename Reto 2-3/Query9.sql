use Chinook;

SELECT Distinct p.Name
FROM Playlist as p JOIN PlaylistTrack as pt ON p.PlaylistId = pt.PlaylistId JOIN
Track as t ON pt.TrackId = t.TrackId
WHERE p.PlaylistId IN (SELECT TrackId FROM Track group by TrackId HAVING TrackId >= MAX(TrackId));