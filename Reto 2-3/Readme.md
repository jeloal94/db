### Todas las Querys Hechas y Explicadas

### Query 1
### Obtener las canciones con una duración superior a la media.
```sql
use Chinook;

SELECT Name
FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds)FROM Track);
```

Esta query la hice buscando el nombre de la cancion y en el **WHERE** fue donde realice la subconsulta poniendo la columna de milisegundos fuera mas grande que la media de las canciones, para sacar las medias de las canciones hice una subconsulta usando el comando **AVG** y asi es como puedo hacer la comparacion en el **WHERE** y realizar la consulta.

### Query 2
### Listar las 5 últimas facturas del cliente cuyo email es “emma_jones@hotmail.com”.
```sql
use Chinook;

SELECT *
FROM Invoice 
WHERE CustomerId = (SELECT CustomerId FROM Customer WHERE Email = "emma_jones@hotmail.com")
ORDER BY InvoiceDate DESC
LIMIT 5;
```

Esta query la hice buscando la fecha de las facturas de la tabla **Invoice** y luego en el **WHERE** busque por la columna **CustomerId** una que fuera igual al resultado de la subconsulta, en la subconsulta hice una busqueda del **CustomerId** de la tabla **Customer** y en el **WHERE** busque un email de la columna **Email** que coincidiera con el del cliente que buscamos y asi poder hacer realizar la consulta, hice un **ORDER BY** de las facturas por la fecha de las facturas de forma desciendente para que me saliera las ultimas primero y le puse un **LIMIT** de 5 porque solo queriamos las 5 ultimas.

### Query 3
### Mostrar las listas de reproducción en las que hay canciones de reggae

```sql
use Chinook;

SELECT Name
FROM Playlist
WHERE EXISTS (SELECT Name FROM Genre WHERE Name = "reggae");
```

Esta query la hice buscando el nombre de las **Playlist**, luego en el **WHERE** use el comando **EXISTS** y hice una subconsulta donde busque el nombre del **GENRE** que fuera **reggae**, y con esto solo me saldran el nombre de las **PlayList** en las que exista el genero de **reggae**.

### Query 4
### Obtener la información de los clientes que han realizado compras superiores a 20€

```sql
use Chinook;

SELECT c.FirstName,c.Company,c.Address
FROM Customer as c join(SELECT CustomerId FROM Invoice WHERE Total < 20) as invoicecus ON c.CustomerId = invoicecus.CustomerId;
```

Esta query la realice buscando el nombre,la compañia y la direccion de los **CLientes**,luego hice un inner join donde hice primero una subconsulta buscando el id de los clientes de la tabla de **Invoice** donde el Total de lo gastado fuera superior a los 20 euros, le puse el alias de invoicecus y luego los uni usando la clave primaria de **Customer** y la clave foranea del mismo nombre de **Invoice** y asi solo me saldrian los nombres, las compañias y las direcciones de los clientes que han realizado compras superiores a 20 euros.

### Query 5
### Álbumes que tienen más de 15 canciones, junto a su artista

```sql
use Chinook;

SELECT a.Title, ar.Name
FROM Album as a JOIN Artist ar ON a.ArtistId = ar.ArtistId
WHERE a.AlbumId IN (SELECT AlbumId FROM Track GROUP BY AlbumId HAVING COUNT(TrackId) > 15);
```

Esta query la realice buscando el titulo del **Album** y el nombre del **Artista**, hice un **INNER JOIN** de **Album** a la tabla **Artista** usando la clave foranea de la tabla **Album** que es ArtistId y la clave principal de la tabla **Artista** que es la misma luego en el **WHERE** use el comando **IN** para que me saliera los id de los albumes que estaban dentro de la subconsulta que realice buscando el id de Album de la tabla **Track** usando un **GROUP BY** por id del album y usando un **HAVING** haciendo un **COUNT** de las canciones y que el total sea superior a 15.

### Query 6
### Obtener los álbumes con un número de canciones superiores a la media.

```sql
use Chinook;

SELECT distinct a.*
FROM Album as a JOIN Track as t ON a.AlbumId = t.AlbumId
WHERE a.AlbumId IN (SELECT AlbumId FROM Track GROUP BY TrackId HAVING TrackId >= AVG(TrackId));
```

Esta query la realice buscando todo de la tabla **Album**, hice un **INNER JOIN** con la tabla **Track** usando la clave primaria de la tabla **Album** con la clave foranea de la tabla **Track** que es la misma luego hice un **WHERE** usando el comando **IN** para que me salgan los id de los albumes que esten dentro de la subconsulta.

### Query 7
### Obtener los álbumes con una duración total superior a la media.

```sql
use Chinook;

SELECT DISTINCT a.*
FROM Album as a JOIN Track as t ON a.AlbumId = t.AlbumId
WHERE a.AlbumId IN (SELECT Albumid FROM Track Group by AlbumId HAVING Milliseconds > AVG(milliseconds));
```

Esta query es muy parecida a la anterior, los unicos cambios estan en la subconsulta en el Group By que se ordenan por el AlbumId y en el **HAVING** usamos la columna de **Milliseconds**.

### Query 8
### Canciones del género con más canciones.

```sql
use Chinook;

SELECT t.Name
FROM Track as t JOIN Genre as g ON t.GenreId = g.GenreId
WHERE t.GenreId IN(SELECT GenreId FROM Genre group by GenreId HAVING GenreId > COUNT(GenreId));
```

En esta query, busque por el nombre de la cancion, hice un **INNER JOIN** de la tabla **Track** a la tabla **Genre**, y en el **WHERE** use la columna de GenreId de la tabla **Track** usando el comando **IN** y una subconsulta que use la columna GenreId de la tabla **Genre**, hice un **GROUP BY** por el GenreId y un **HAVING** en el que buscaba el GenreId mas grande que el **COUNT** del GenreId, y asi es como consegui los nombres de las canciones del genero con mas canciones.

### Query 9
### Canciones de la playlist con más canciones.

```sql
use Chinook;

SELECT Distinct p.Name
FROM Playlist as p JOIN PlaylistTrack as pt ON p.PlaylistId = pt.PlaylistId JOIN
Track as t ON pt.TrackId = t.TrackId
WHERE p.PlaylistId IN (SELECT TrackId FROM Track group by TrackId HAVING TrackId >= MAX(TrackId));
```

En esta query, busque el nombre de las Playlist, hice dos **INNER JOINS** el primero de la tabla **Playlist** a la tabla **PlaylistTrack** y luego a la tabla **Track** desde **PlaylistTrack**, luego use un **WHERE** con la columna de **PlaylistId** de la tabla **Playlist** usando el comando **IN** donde hice una subconsulta buscando el TrackId de la tabla **Track** realice un **GROUP BY** por el TrackId luego realice un **HAVING** de TrackId mas grande del maximo de todos los TrackId.

### Query 10
### Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

```sql
use Chinook;

SELECT c.FirstName, (SELECT SUM(il.UnitPrice * il.Quantity)
FROM Invoice AS i JOIN InvoiceLine as il ON i.InvoiceId = il.InvoiceId
WHERE i.CustomerId = c.CustomerId) as "Total Gastado"
FROM Customer as c;
```

Para realizar en esta query, hice una subconsulta en el **SELECT** en la que me hiciera una suma de las columnas **UnitPrice** y **Quantity** de la tabla **InvoiceLine** hice un **INNER JOIN** de la tabla **Invoice** a la tabla **InvoiceLine** y luego realice un **WHERE** en el cual tenian que coincidir la columna de **CustomerId** de la tabla **Invoice** y de la tabla **Customer** usando el **Alias** del **FROM** de afuera de la subconsulta que es de la tabla **Customer**.

### Query 11
### Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

```sql
use Chinook;

SELECT e.FirstName,(SELECT COUNT(CustomerId)
FROM Customer as c WHERE c.SupportRepId = e.EmployeeId) as "Clientes"
FROM Employee as e;
```

Para realizar esta query, hice una subconsulta en el **SELECT**, en la cual hice un **COUNT** de la columna **CustomerId** y hice un **WHERE** comparando que la clave secundaria de la tabla **Customer** que es **SupportRepId**, fuera igual al **EmployeeId** de la tabla de **Employee** que es la tabla donde estoy realizando esta consulta.

### Query 12
### Ventas totales de cada empleado.

```sql
use Chinook;

SELECT e.FirstName, (SELECT COUNT(i.InvoiceId) FROM Invoice AS i JOIN
Customer as c ON i.CustomerId = c.CustomerId WHERE c.SupportRepId = e.EmployeeId) as Ventas
FROM Employee as e;
```

Para realizar esta query, hice una subconsulta en el **SELECT** realizando dentro de esta un **COUNT** de la tabla **Invoice** luego hice un **INNER JOIN** con la tabla **Customer** y luego realizo un **WHERE** con la clave secundaria de la tabla **Customer** y la comparo con la principal de **Employee**, que es la tabla donde realizo la consulta.

### Query 13
### Álbumes junto al número de canciones en cada uno.

```sql
USE Chinook;

SELECT a.Title, (SELECT COUNT(TrackId) FROM Track as t WHERE t.AlbumId = a.AlbumId) as Canciones
FROM Album as a;
```

Esta query se realiza de una forma rapida, usando una subconsulta dentro del **SELECT** de tu primera consulta, en la subconsulta uso un **COUNT** de **TrackId** de la tabla Track y luego uso un **WHERE** donde quiero que la id de la tabla **Track** coincida con la columna **AlbumId** de la tabla **Album**.

### Query 14
### Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

```sql
use Chinook;

SELECT ar.Name, a.Title
FROM Artist as ar JOIN(SELECT AlbumId,ArtistId,Title FROM Album ORDER BY AlbumId DESC) as a ON ar.ArtistId = a.ArtistId;
```

En la ultima query, busco el **Name** de la tabla **Artist**,y el **Title** de la tabla **Album**, y en el **FROM** a la hora de hacer un **INNER JOIN** hice una subconsulta dentro donde realice un **SELECT** buscando la columna AlbumId, ArtistId y Title de la tabla Album y realice un **ORDER BY** de AlbumId de forma descendiente, y lo conecte con la tabla Artist por la clave primaria de **Artist** y la clave secundaria de **Album** que tambien es **ArtistId**.