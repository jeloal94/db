### 1- Preparacion de la base de datos y sus tablas.
Lo mejor para preparar una base de datos, es saber de que trata la base de datos y que tablas deberiamos poner, por ejemplo en este ejercicio vamos a preparar una base de datos muy sencilla de una agencia de vuelos, lo primero que haremos sera el comando:
```sql
CREATE DATABASE Vuelos;
```
Con el comando **CREATE DATABASE** se crea la base de datos con el nombre que le hayas querido poner, en mi caso yo he puesto el nombre de Vuelos porque es sobre lo que trata esta base de datos. He creado 3 tablas porque necesito una tabla sobre los **vuelos** de donde vuelan y hasta donde llegan, que dia y hora, cuanta capacidad tiene el avion, otra tabla la necesito para guardar informacion de los **pasajeros** como su nombre y numero de pasaporte o dni y la ultima tabla para la informacion de la **reserva** de vuelos donde la tabla de **Vuelo** y la tabla de **Pasajeros** necesitare sus **claves primarias** como **claves foraneas** para poder **relacionar** las tablas entre si.

## Creacion de la tabla vuelo es la tabla donde estara la informacion de los vuelos y la capacidad del avion.
```sql
USE Vuelos;
CREATE TABLE Vuelo(
    id_vuelo PRIMARY KEY AUTO_INCREMENT,
    origen varchar(35) NOT NULL,
    destino varchar(35) NOT NULL,
    fecha datetime NOT NULL,
    capacidad int NOT NULL
);
```
He creado la tabla de Vuelo con una id que se va **autoincrementando** para que sea rapido y ordenado. En la columna de origen y en la de destino hemos puesto un **varchar de 35** porque pueden existir ciudades con nombres muy largos, y asi tenemos mucho espacio para poner el nombre entero y las hemos puesto **NOT NULL** porque todos los vuelos tienen que tener una ciudad de origen y una ciudad destino. En la fecha he decidido un **datetime** porque asi se puede ser mas preciso y poner tambien a la hora que es el vuelo, y tambien la he puesto **NOT NULL** porque todo vuelo necesita una fecha exacta para que los pasajeros puedan saber a que hora sale su vuelo.La capacidad la he puesto un int porque es una forma sencilla de poner cualquier numero sin que de problemas, y tambien es **NOT NULL** porque es una informacion necesaria para saber cuantos pasajeros pueden entrar en el avion. En el caso que se quiera eliminar un vuelo primero se tendrian que eliminar las reservas de los vuelos, si las hubiera. Si se quiere eliminar un vuelo simplemente tendriamos que usar este comando:

```sql
 DELETE FROM Vuelo WHERE id_vuelo = 1;
```
##### Acuerdate de usar siempre el WHERE en el DELETE FROM porque sino tendras problemas que no quieres tener.

## Creacion de la tabla pasajeros es la tabla donde tendremos alguna informacion de los pasajeros que van a subir a nuestros aviones.
```sql
USE Vuelos;
CREATE TABLE Pasajeros(
    id_pasajero PRIMARY KEY AUTO_INCREMENT,
    num_pasaporte varchar(9) NOT NULL,
    nom_pasajero varchar(40) NOT NULL
);
```
He creado la tabla pasajeros usando una id **AUTO_INCREMENT** para que sea rapido y ordenado. Tambien quiero conocer el numero del pasaporte he puesto un **varchar(9)** porque no es demasiado el numero del pasaporte y como es necesario lo he puesto **NOT NULL** y el nombre del pasajero un **varchar(40)** para que pueda entrar los nombre y apellidos de cualquier persona y como me parece informacion importante tambien la he puesto **NOT NULL**.

## Creacion de la tabla de Reservas es la tabla donde se guarda la reservas de los vuelos.
```sql
CREATE TABLE Reserva(
    id_vuelo int,
    id_pasajero int,
    id_reserva int PRIMARY KEY,
    n_asiento varchar(4),
    FOREIGN KEY (id_vuelo) REFERENCES Vuelo(id_vuelo),
    FOREIGN KEY (id_pasajero) REFERENCES Pasajeros(id_pasajero)
);
```
He creado la tabla de Reserva usando las 2 **PRIMARY KEY** de las tablas Vuelos y Pasajeros siendo dos **llaves foraneas** o **FOREIGN KEY** como se le suele decir. **id_reserva** es la **PRIMARY KEY** de la tabla y esta vez no es **AUTO_INCREMENT** porque prefiero que la gente ponga el numero que ellos recuerden bien, y el **n_asiento** es un **varchar(4)** porque los asientos se pondran estilo "A-14".
