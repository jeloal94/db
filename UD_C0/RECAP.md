# Unidad C0: Recapitulación

Autor. Jesus Lopez Alacot

Introducción (sobre el documento o el tema tratado) -- mejor escribirla al final.

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.

Una base de datos se encarga no solo de almacenar datos, sino tambien de conectarlos entre si en una unidad logica.

2.Permite administrar grandes cantidades de informacion. 

### Bibliografia
- Wikipedia


## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? Definición de DBMS.

1.Es un software de sistema para crear y administrar bases de datos, proporciona el método de orgaizacion
para el almacenamiento y recuperación flexible de grandes cantidades de datos.

2- 
Control de acceso- El DBMS debe permitir definir quien tiene acceso a que datos y en que condiciones. Esto implica la capacidad de establecer permisos a nivel de usuario o de roles para restringir el acceso a datos sensibles.

Integridad de los datos - El sistema debe garantizar que los datos almacenados sean precisos y consistentes.

Transacciones ACID - Debe admitir transacciones ACID(Atomicidad,Consistencia,Aislamiento,Durabilidad) para garantizar que las operaciones de la base de datos sean confiables y seguras,incluso en entornos concurrentes.

Lenguaje de consulta - Debería proporcionar un lenguaje de consulta poderoso y eficiente para recuperar, manipular y gestionar los datos almacenados. SQL (Structured Query Language) es el lenguaje de consulta más comúnmente utilizado en los DBMS relacionales.

Optimización de consultas - Debe tener capacidades de optimización de consultas para ejecutar las consultas de manera eficiente y mejorar el rendimiento del sistema.

Backup y recuperación - Debería permitir realizar copias de seguridad regulares de la base de datos para proteger los datos contra pérdidas y proporcionar mecanismos de recuperación en caso de fallos del sistema o errores humanos

Replicación y sincronización - Puede ofrecer capacidades de replicación y sincronización para distribuir los datos en múltiples ubicaciones y garantizar la disponibilidad y la redundancia.

Seguridad - Debería proporcionar funciones de seguridad avanzadas, como el cifrado de datos, la auditoría de acceso y la gestión de identidades, para proteger los datos contra accesos no autorizados y cumplir con las regulaciones de privacidad y seguridad.

Escalabilidad - Debe ser capaz de escalar para manejar grandes volúmenes de datos y un alto número de usuarios concurrentes sin comprometer el rendimiento.

Soporte para diferentes modelos de datos - Además de los modelos relacionales, algunos DBMS también pueden proporcionar soporte para otros modelos de datos, como el modelo de documentos, el modelo de grafo o el modelo de clave-valor.
### Bibliografia
- Wikipedia
- ChatGPT

### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* Oracle DB - Si, No es software libre, Si lo sigue
* IMB Db2 - Si, No es software libre, Si lo sigue
* SQLite - Si, si es software libre, No lo sigue
* MariaDB - Si, si es software libre, Si lo sigue
* SQL Server - No, no es software libre, Si lo sigue
* PostgreSQL - Si, si es software libre, Si lo sigue
* mySQL - Si, si es software libre, Si lo sigue

## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor? ¿Qué ventajas tiene desacoplar al DBMS del cliente? ¿En qué se basa el modelo cliente-servidor?
1-
Centralización de datos: Al tener el DBMS en un servidor dedicado, todos los datos están centralizados en un solo lugar. Esto facilita la gestión y la administración de los datos, ya que no es necesario replicar la base de datos en múltiples ubicaciones o mantener varias copias actualizadas.

Mejor rendimiento: Los servidores dedicados suelen tener recursos de hardware más potentes, como procesadores de alto rendimiento, gran cantidad de memoria RAM y discos rápidos en RAID. Esto permite que el DBMS funcione de manera más eficiente y maneje un mayor volumen de consultas y transacciones concurrentes.

Seguridad: Los servidores dedicados pueden implementar medidas de seguridad más estrictas, como firewalls, sistemas de detección de intrusiones y mecanismos de autenticación avanzados. Esto ayuda a proteger los datos almacenados en la base de datos contra accesos no autorizados y ataques informáticos.

Escalabilidad: Los servidores dedicados pueden escalar verticalmente (aumentando la capacidad de hardware) o horizontalmente (agregando más servidores) según sea necesario para satisfacer la demanda de recursos del DBMS. Esto permite que la base de datos crezca con el tiempo y se adapte a las necesidades cambiantes de la organización.

Disponibilidad: Los servidores dedicados suelen estar configurados con redundancia de hardware y sistemas de respaldo para garantizar una alta disponibilidad de la base de datos. Esto significa que incluso en caso de falla de hardware o mantenimiento planificado, la base de datos sigue estando accesible para los usuarios.

Gestión centralizada: Al tener el DBMS en un servidor dedicado, la gestión y la administración de la base de datos se simplifican, ya que todos los recursos y configuraciones están centralizados en un solo lugar. Esto facilita las tareas de copia de seguridad, monitoreo, ajuste de rendimiento y actualizaciones de software.

2-
Portabilidad: Al desacoplar el DBMS del cliente, el software cliente puede ser independiente del sistema operativo y del entorno de ejecución del servidor de base de datos. Esto facilita la portabilidad del software cliente, ya que puede ejecutarse en diferentes plataformas sin necesidad de modificar el código para adaptarse a diferentes sistemas de gestión de bases de datos.

Escalabilidad: Desacoplar el DBMS del cliente permite escalar horizontalmente fácilmente. Puedes agregar más clientes sin tener que preocuparte por aumentar la carga en el servidor de base de datos. Esto es especialmente útil en aplicaciones web o móviles donde el número de usuarios puede variar drásticamente.

Seguridad: Al mantener el DBMS detrás de una capa de servidor, puedes implementar medidas de seguridad más robustas. Por ejemplo, puedes restringir el acceso al DBMS solo a través de interfaces seguras y controladas, lo que reduce el riesgo de exposición de la base de datos a posibles amenazas externas.

Rendimiento: Al desacoplar el DBMS del cliente, puedes optimizar el rendimiento de la aplicación mediante el uso de cachés en el cliente, lo que reduce la necesidad de realizar múltiples consultas al servidor de base de datos. Además, puedes implementar técnicas de paginación y pre-carga de datos en el cliente para mejorar la experiencia del usuario y reducir la carga en el servidor de base de datos.

Flexibilidad: Desacoplar el DBMS del cliente permite implementar diferentes tipos de clientes para diferentes propósitos sin tener que cambiar la infraestructura subyacente de la base de datos. Por ejemplo, puedes tener clientes web, clientes móviles y clientes de escritorio que se conectan al mismo servidor de base de datos, lo que proporciona una mayor flexibilidad en el desarrollo y la implementación de la aplicación.

3-
El modelo cliente-servidor se basa en una estructura de red donde los clientes solicitan servicios y los servidores los proporcionan. Los clientes son sistemas que solicitan recursos, como aplicaciones en computadoras o dispositivos móviles.

* __Cliente__: Sistema que solicita recursos o servicios.
* __Servidor__: Sistema que proporciona recursos o servicios a los clientes.
* __Red__: Medio de comunicación que conecta clientes y servidores.
* __Puerto de escucha__: Número de identificación utilizado por el servidor para recibir solicitudes de los clientes.
* __Petición__:  Solicitud enviada por el cliente al servidor para obtener recursos o servicios.
* __Respuesta__:  Datos devueltos por el servidor en respuesta a una solicitud del cliente.
### Bibliografia
- Wikipedia
- ChatGpt
## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?
SQL (Structured Query Language) es un lenguaje de programación estándar utilizado para comunicarse con sistemas de gestión de bases de datos relacionales (RDBMS). Fue desarrollado originalmente por IBM en la década de 1970 y desde entonces se ha convertido en el lenguaje estándar para interactuar con bases de datos.
### Instrucciones de SQL
Consultas de datos: Permite recuperar información de la base de datos utilizando comandos como SELECT.
Modificación de datos: Permite insertar, actualizar y eliminar registros en una tabla.
Definición de datos: Permite crear, modificar y eliminar tablas, índices y otros objetos de la base de datos.
Control de acceso: Permite definir permisos de acceso y gestionar la seguridad de la base de datos.
#### DDL
DDL (Data Definition Language) es un subconjunto del lenguaje SQL (Structured Query Language) utilizado para definir, modificar y eliminar estructuras de datos en una base de datos. Las sentencias DDL permiten a los usuarios definir la estructura de la base de datos, incluyendo tablas, índices, restricciones y otros objetos relacionados
#### DML
DML (Data Manipulation Language) es un subconjunto del lenguaje SQL (Structured Query Language) que se utiliza para manipular datos en una base de datos relacional.
#### DCL
DCL (Data Control Language) es un subconjunto del lenguaje SQL (Structured Query Language) que se utiliza para controlar el acceso a los datos en una base de datos relacional.
#### TCL
TCL (Transaction Control Language) es un subconjunto del lenguaje SQL (Structured Query Language) que se utiliza para controlar las transacciones en una base de datos relacional. 
### Bibliografia
- Wikipedia
- ChatGpt

## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?
1-
Una base de datos relacional es un tipo de sistema de gestión de bases de datos (DBMS) que organiza los datos en tablas relacionadas entre sí. Estas relaciones se establecen mediante claves primarias y claves externas. El modelo relacional permite realizar consultas complejas utilizando el lenguaje SQL y garantiza la integridad de los datos mediante reglas de integridad referencial. Las bases de datos relacionales son ampliamente utilizadas en aplicaciones empresariales debido a su flexibilidad, eficiencia y capacidad para manejar grandes volúmenes de datos. Ejemplos de sistemas de gestión de bases de datos relacionales incluyen MySQL, PostgreSQL, Oracle y SQL Server.

2-
Estructura organizada: Los datos se organizan en tablas, lo que facilita la comprensión y la gestión de la información.

Integridad de datos: El modelo relacional permite establecer relaciones entre las tablas mediante claves primarias y foráneas, lo que garantiza la integridad referencial y evita la inconsistencia de los datos.

Flexibilidad en consultas: Las bases de datos relacionales permiten realizar consultas complejas utilizando el lenguaje SQL (Structured Query Language), lo que facilita la recuperación de información específica según las necesidades del usuario.

Escalabilidad: Pueden manejar grandes volúmenes de datos y admitir un alto número de usuarios concurrentes, lo que las hace adecuadas para aplicaciones de gran escala.

Mantenimiento de la consistencia: Las transacciones ACID (Atomicidad, Consistencia, Aislamiento, Durabilidad) garantizan que las operaciones realizadas en la base de datos sean confiables y consistentes, incluso en entornos concurrentes.

Seguridad: Permiten establecer controles de acceso para proteger la información sensible y restringir el acceso no autorizado a los datos.

Compatibilidad y estándares: El lenguaje SQL es un estándar ampliamente aceptado en la industria, lo que facilita la portabilidad y la interoperabilidad entre diferentes sistemas de gestión de bases de datos relacionales.

3-
Tablas: Las tablas son la estructura principal de una base de datos relacional. Cada tabla representa una entidad o conjunto de entidades, y está compuesta por filas y columnas. Cada fila corresponde a un registro individual, mientras que cada columna representa un atributo específico de esos registros.

Claves primarias: Las claves primarias son atributos o conjuntos de atributos que identifican de forma única cada fila en una tabla. Se utilizan para garantizar la unicidad de los registros y establecer relaciones con otras tablas.

Claves foráneas: Las claves foráneas son atributos que establecen relaciones entre tablas. Representan las referencias a las claves primarias de otras tablas y se utilizan para mantener la integridad referencial entre los datos.

Relaciones: Las relaciones definen la conexión lógica entre las tablas de la base de datos. Pueden ser de uno a uno, uno a muchos o muchos a muchos, y se establecen mediante claves primarias y foráneas.

Índices: Los índices son estructuras adicionales que se utilizan para acelerar la búsqueda y recuperación de datos en una base de datos. Se crean en columnas específicas de las tablas y permiten realizar consultas de manera más eficiente.

Restricciones de integridad: Las restricciones de integridad son reglas que se aplican a los datos para garantizar su coherencia y consistencia. Incluyen restricciones de clave primaria, restricciones de clave foránea, restricciones de unicidad y restricciones de verificación.

Vistas: Las vistas son consultas almacenadas que permiten acceder a los datos de una o más tablas como si fueran una tabla única. Se utilizan para simplificar consultas complejas y proporcionar una capa adicional de seguridad.

Procedimientos almacenados y funciones: Son conjuntos de instrucciones SQL que se almacenan en la base de datos y se pueden llamar desde otras aplicaciones o consultas SQL. Ayudan a modularizar la lógica de negocio y mejorar el rendimiento de la base de datos.

* __Relación (tabla)__: Estructura que organiza datos en filas y columnas
* __Atributo/Campo (columna)__: Cada columna representa un tipo específico de información en una tabla.
* __Registro/Tupla (fila)__:  Cada fila contiene un conjunto de valores que representan una entidad única en la tabla.

### Bibliografia
- Wikipedia
- ChatGPT

