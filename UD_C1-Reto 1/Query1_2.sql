use sanitat;
-- Query 2
-- Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre
select NOM, HOSPITAL_COD, TELEFON
from HOSPITAL
where substring(NOM , 2, 1)  = "A";