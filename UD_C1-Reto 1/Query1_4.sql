use sanitat;
-- Query 4

-- Muestre los trabajadores (código hospital, código sala, número empleado y
-- apellido) que no sean del turno de noche.

SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM
FROM PLANTILLA
WHERE TORN != "N";