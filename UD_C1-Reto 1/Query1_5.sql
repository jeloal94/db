use sanitat;
-- Query 5
-- Muestre a los enfermos nacidos en 1960
SELECT *
FROM MALALT
WHERE DATA_NAIX BETWEEN '1960-01-01' AND '1960-12-31';