USE sanitat;
-- Query 6
-- Muestre a los enfermos nacidos a partir del año 1960
SELECT *
FROM MALALT
WHERE DATA_NAIX > DATE('1960-01-01');