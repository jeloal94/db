### Si quieres ver las querys accede a este url
 https://gitlab.com/jeloal94/db/-/tree/main/UD_C1-Reto%201?ref_type=heads

### Muestre los hospitales existentes (número, nombre y teléfono).

```sql
use sanitat;

select NOM, HOSPITAL_COD, TELEFON
from HOSPITAL;
```

### Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre. 
```sql
use sanitat;

select NOM, HOSPITAL_COD, TELEFON
from HOSPITAL
where substring(NOM , 2, 1)  = "A";
```

###  Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.
```sql
use sanitat;

select HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM
FROM PLANTILLA;
```

###  Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.
```sql
use sanitat;

SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM
FROM PLANTILLA
WHERE TORN != "N";
```

###  Muestre a los enfermos nacidos en 1960.
```sql
use sanitat;

SELECT *
FROM MALALT
WHERE DATA_NAIX BETWEEN '1960-01-01' AND '1960-12-31';
```

### Muestre a los enfermos nacidos a partir del año 1960.
```sql
use sanitat;

SELECT *
FROM MALALT
WHERE DATA_NAIX > DATE('1960-01-01');
```