use empresa;

-- Query4
-- Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636

SELECT CLIENT_COD, NOM, CIUTAT
FROM CLIENT
WHERE AREA != 636;
