Este README esta hecho por Jesus Lopez Alacot
Aqui se muestra todos los Querys realizados para el ejercicio de Empresa
### Los Querys estan en este link de Git Lab
https://gitlab.com/jeloal94/db/-/tree/main/UD_C1-Reto%202/Empresa?ref_type=heads
### Muestre los productos (código y descripción) que comercializa la empresa.
```sql
use empresa;
SELECT PROD_NUM, DESCRIPCIO
FROM PRODUCTE;

```

### Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción.
```sql
use empresa;

SELECT PROD_NUM, DESCRIPCIO
FROM PRODUCTE
WHERE DESCRIPCIO LIKE '%TENNIS%';

``` 

### Muestre el código, nombre, área y teléfono de los clientes de la empresa.

```sql
use empresa;

SELECT CLIENT_COD, NOM, AREA, TELEFON
FROM CLIENT;
``` 

### Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.

```sql
use empresa;

SELECT CLIENT_COD, NOM, CIUTAT
FROM CLIENT
WHERE AREA != 636;
``` 

### Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío).
```sql
use empresa;

SELECT COM_NUM,COM_DATA,DATA_TRAMESA
FROM COMANDA;
``` 
### ¿En que años hemos dado de alta a trabajadores?
```sql
use empresa;

SELECT distinct YEAR(DATA_ALTA)
FROM EMP;

-- Distinct devuelve valores diferentes pero no se repiten
``` 

### ¿Que categorias de empleados tengo actualmente?
```sql
use empresa;

SELECT DISTINCT OFICI
FROM EMP;
``` 

### ¿Cuantas son en total?
```sql
use empresa;

SELECT COUNT(DISTINCT OFICI)
FROM EMP;
``` 

### ¿Que empleados van a comision?
```sql
use empresa;

SELECT *
FROM EMP
WHERE COMISSIO IS NOT NULL;
``` 

### ¿Que dos empleados se llevan la mayor comision?
```sql
use empresa;

SELECT *
FROM EMP
WHERE COMISSIO IS NOT NULL
ORDER BY COMISSIO DESC
LIMIT 2;
``` 