Este README esta hecho por Jesus Lopez Alacot
Aqui se muestra todos los Querys realizados para el ejercicio de Videoclub
### Los Querys estan en este link de Git Lab
https://gitlab.com/jeloal94/db/-/tree/main/UD_C1-Reto%202/Videoclub?ref_type=heads
### Lista de nombres y teléfonos de los clientes.
```sql
use videoclub;

SELECT NOM,TELEFON
FROM CLIENT;
```

### Lista de fechas e importes de las facturas.
```sql
use videoclub;

SELECT Data, Import
FROM FACTURA;
```

### Lista de productos (descripción) facturados en la factura número 3.
```sql
use videoclub;

SELECT Descripcio
FROM DETALLFACTURA
WHERE CodiFactura = 3;
```

### Lista de facturas ordenada de forma decreciente por importe.
```sql
use videoclub;

SELECT *
FROM FACTURA
ORDER BY Import DESC;
```

### Lista de los actores cuyo nombre comience por X.
```sql
use videoclub;

SELECT *
FROM ACTOR
WHERE Nom LIKE 'X%';
```

### Añado 2 actores que empiezan por la letra X.
```sql
use videoclub;

INSERT INTO ACTOR
VALUES(5,"Xavi"),
(6,"Xordi");
```

### Eliminar a Xordi.
```sql
use videoclub;

DELETE FROM ACTOR WHERE Nom = "Xordi";
```