### Este readme tiene todos los Querys del Reto 3 creado por Jesus Lopez Alacot
El codigo de gitlab para acceder a los Querys es este 
### Query 1
### Lista todas las películas del videoclub junto al nombre de su género.

```sql
use videoclub;

SELECT p.Titol,g.Descripcio
FROM PELICULA p INNER JOIN GENERE g ON p.CodiGenere = g.CodiGenere;
```
### Query 2
### Lista todas las facturas de María.

```sql
SELECT f.*
FROM FACTURA f INNER JOIN CLIENT c ON f.DNI = c.DNI
WHERE c.Nom LIKE "Maria %";
```

### Query 3
### Lista las películas junto a su actor principal.
```sql
use videoclub;

SELECT p.Titol,a.Nom
FROM PELICULA p INNER JOIN ACTOR a ON p.CodiActor = a.CodiActor;
```

### Query 4
### Lista películas junto a todos los actores que la interpretaron
```sql
use videoclub;

SELECT p.Titol,i.CodiActor,a.Nom
FROM PELICULA p JOIN INTERPRETADA i ON p.CodiPeli = i.CodiPeli
INNER JOIN ACTOR a ON i.CodiActor = a.CodiActor;
```
#### El nombre de los actores lo he puesto para ver mejor quienes son los actores que estan en las peliculas

### Query 5
### Lista ID y nombres de las películas junto a los ID y nombres de sus segundas partes.
```sql
use videoclub;

SELECT p.CodiPeli,p.Titol,p1.Codipeli,p1.SegonaPart
FROM PELICULA p INNER JOIN PELICULA p1 ON p.CodiPeli = p1.CodiPeli;
```